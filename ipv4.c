#include "ipv4.h"

// https://codereview.stackexchange.com/questions/149717/implementation-of-c-standard-library-function-ntohl
#include <stdint.h>
#include <string.h>

uint32_t ntohl(uint32_t const net) {
    uint8_t data[4] = {};
    memcpy(&data, &net, sizeof(data));

    return ((uint32_t) data[3] << 0)
         | ((uint32_t) data[2] << 8)
         | ((uint32_t) data[1] << 16)
         | ((uint32_t) data[0] << 24);
}

uint16_t ntohs(uint16_t const net) {
    uint8_t data[2] = {};
    memcpy(&data, &net, sizeof(data));

    return ((uint32_t) data[1] << 0)
         | ((uint32_t) data[0] << 8);
}

// https://en.wikipedia.org/wiki/EtherType
// https://en.wikipedia.org/wiki/IEEE_802.1Q
typedef struct __attribute__((packed)) EthernetFrame // BigEndian
{  
    /* https://en.wikipedia.org/wiki/Ethernet_frame */
    /* Layer 2 does not contain preamble */
    uint8_t dest_MAC[6];
    uint8_t src_MAC[6];
    uint16_t type;
    // uint8_t payload[];
    // uint8_t crc[4]
}
EthernetFrame;

typedef struct __attribute__((packed)) Ipv4HeaderNetwork // BigEndian
{  
        uint32_t IHL              :4;
        uint32_t version          :4;
        uint32_t DSCP             :6;
        uint32_t ECN              :2;
        uint32_t total_length    :16;
        
        uint32_t identification  :16;
        uint32_t flags           :3;
        uint32_t fragment_offset :13;

        uint32_t time_to_live    :8;
        uint32_t protocol        :8;
        uint32_t header_checksum :16;
}
Ipv4HeaderNetwork;

bool Ipv4Check (const void* buffer, size_t bufLen, Ipv4Info* info)
{
    #define MIN_LEN 20
    #define MAX_LEN 65535
    if ( bufLen < MIN_LEN)
        return false;
    if ( bufLen > MAX_LEN)
        return false;

    // Unalligned memory access for some CPUs can be an issue, Cortex-M0.
    // We don't check for aligment currently.
    const struct Ipv4HeaderNetwork * frame = buffer;
    if ( frame->version != 4 )
        return false;

    const uint16_t total_length = ntohs(frame->total_length);
    if ( total_length != bufLen )
        return false;

    if (bufLen < (frame->IHL * 4)) // ihl * 32bit / 8bit@byte = ihl * 4
        return false;

    info->dscp = frame->DSCP;
    info->protocol = frame->protocol;
    info->optionsPresent = frame->IHL > 5 && bufLen > MIN_LEN;

    return true;    
}


bool ethIpv4Parse (const void* buffer, size_t bufLen, Ipv4Info* info)
{
    enum EtherType
    {
        VLAN_QinQ = 0x88a8, // Multiple VLAN tags https://en.wikipedia.org/wiki/IEEE_802.1ad
        VLAN =      0x8100, // VLAN-tagged frame (IEEE 802.1Q)
        IPv4 =      0x0800
    };
    if ( bufLen < 60) /* Wireshark valid frames are without CRC, last 4 bytes */
        return false;
    
    const struct EthernetFrame * frame = buffer;
    const uint8_t * type = (const uint8_t *) &frame->type;
    size_t vlan_offset = 0;

    /* We can dereference pointer 1st time because of min size requirement */
    while (ntohs(*(uint16_t *)(type + vlan_offset)) == VLAN_QinQ )
    {
        vlan_offset += 4;
        const uint16_t expected_buffer_end = offsetof(EthernetFrame, type) \
                                       + vlan_offset \
                                       + 4 /* VLAN tag */
                                       + 2 /* Classic EtherType */ \
                                       + 4 /*crc*/;
        if (expected_buffer_end > bufLen)
            return false;

    }
    /* After VLAN_QinQ tags we expect one VLAN EtherTag */
    if (vlan_offset > 0 && ntohs(*(uint16_t *)(type + vlan_offset)) != VLAN) 
        return false;

    if (ntohs(*(uint16_t *)(type + vlan_offset)) == VLAN)
    {
        vlan_offset += 4;
    }

    if (ntohs(*(uint16_t *)(type + vlan_offset)) != IPv4) 
        return false;
    
    const size_t payload_offset = vlan_offset + sizeof(struct EthernetFrame);
    const int16_t buffer_size = bufLen - payload_offset;

    if ( buffer_size < 0 )
        return false;

    const void * buff = (uint8_t *) buffer + payload_offset;

    return Ipv4Check (buff, (size_t) buffer_size, info);
}