#include <stdio.h>
#include "ipv4.h"
#include <stdbool.h>
#include <assert.h>

bool assert_check()
{
    printf("%s\n", __FUNCTION__);
    return false;
}

bool IPv4_frame_length_more_than_20()
{
    printf("%s\n", __FUNCTION__);
    const uint8_t buf[19]= {};
    const size_t buf_len = sizeof(buf);
    struct Ipv4Info info; 
    return Ipv4Check ( buf, buf_len, &info);
}

bool IPv4_frame_length_less_than_65535()
{
    printf("%s\n", __FUNCTION__);
    const uint8_t buf[65536]= {};
    const size_t buf_len = sizeof(buf);
    struct Ipv4Info info; 
    return Ipv4Check ( buf, buf_len, &info);
}

bool IPv4_version_4()
{
    printf("%s\n", __FUNCTION__);
    const uint8_t buf[20]= {
        [0] = 3,
    };
    const size_t buf_len = sizeof(buf);
    struct Ipv4Info info; 
    return Ipv4Check ( buf, buf_len, &info);
}

bool IPv4_total_length_same_to_buff_size()
{
    printf("%s\n", __FUNCTION__);
    #define LENGTH 20
    const uint8_t buf[LENGTH]= {
        [0] = 4,
        [2] = 0,
        [3] = LENGTH - 1,
    };
    const size_t buf_len = sizeof(buf);
    struct Ipv4Info info; 
    return Ipv4Check ( buf, buf_len, &info);
}

bool IPv4_protocol_check()
{
    printf("%s\n", __FUNCTION__);
    #define LENGTH 20
    // https://en.wikipedia.org/wiki/List_of_IP_protocol_numbers
    // 0x11	17	UDP	User Datagram Protocol	RFC 768
    const uint8_t protocol = 0x11;
    const uint8_t buf[LENGTH]= {
        [0] = 4,
        [2] = 0,
        [3] = LENGTH,
        [9] = protocol,
    };
    const size_t buf_len = sizeof(buf);
    struct Ipv4Info info; 
    if ( true == Ipv4Check ( buf, buf_len, &info) &&
         protocol == info.protocol )
    {
        return true;
    }
    return false;
    #undef LENGTH
}

bool IPv4_options_are_present()
{
    printf("%s\n", __FUNCTION__);
    #define LENGTH 24
    // https://en.wikipedia.org/wiki/List_of_IP_protocol_numbers
    // 0x11	17	UDP	User Datagram Protocol	RFC 768
    const uint8_t protocol = 0x11;
    const uint8_t ihl = 6;
    const uint8_t buf[LENGTH]= {
        [0] = 4 | (ihl << 4),
        [2] = 0,
        [3] = LENGTH,
        [9] = protocol,
    };
    const size_t buf_len = sizeof(buf);
    struct Ipv4Info info; 
    if ( true == Ipv4Check ( buf, buf_len, &info) &&
         protocol == info.protocol &&
         info.optionsPresent)
    {
        return true;
    }
    return false;
    #undef LENGTH
}

bool IPv4_as_payload_of_Ethernet_frame_without_VLAN()
{
    printf("%s\n", __FUNCTION__);
    #define PAYLOAD_OFFSET (14 + 4*0) 
    #define IPV4_LENGTH 46
    #define CRC_SIZE 4
    const uint8_t protocol = 0x11;
    const uint8_t ihl = 6;
    const uint16_t type = 0x0800;

    const uint8_t buf[PAYLOAD_OFFSET + IPV4_LENGTH + CRC_SIZE]= {
        
        [PAYLOAD_OFFSET - 2] = ((uint8_t *)&type)[1],
        [PAYLOAD_OFFSET - 1] = ((uint8_t *)&type)[0],

        [PAYLOAD_OFFSET + 0] = 4 | (ihl << 4),
        [PAYLOAD_OFFSET + 2] = 0,
        [PAYLOAD_OFFSET + 3] = IPV4_LENGTH,
        [PAYLOAD_OFFSET + 9] = protocol,
    };
    const size_t buf_len = sizeof(buf);
    struct Ipv4Info info; 
    if ( true == ethIpv4Parse ( buf, buf_len, &info) &&
         protocol == info.protocol &&
         info.optionsPresent)
    {
        return true;
    }
    return false;

    #undef PAYLOAD_OFFSET
    #undef IPV4_LENGTH
    #undef CRC_SIZE
}

bool IPv4_as_payload_of_Ethernet_frame_with_1_VLAN_ethertype()
{
    // https://en.wikipedia.org/wiki/IEEE_802.1Q
    printf("%s\n", __FUNCTION__);
    #define PAYLOAD_OFFSET (14 + 4*1) 
    #define IPV4_LENGTH 46
    #define CRC_SIZE 4
    const uint8_t protocol = 0x11;
    const uint8_t ihl = 6;
    const uint16_t vlan_type = 0x8100;
    const uint16_t type = 0x0800;

    const uint8_t buf[PAYLOAD_OFFSET + IPV4_LENGTH + CRC_SIZE]= {

        [PAYLOAD_OFFSET - 1*4 - 2] = ((uint8_t *)&vlan_type)[1],
        [PAYLOAD_OFFSET - 1*4 - 1] = ((uint8_t *)&vlan_type)[0],
        
        [PAYLOAD_OFFSET - 2] = ((uint8_t *)&type)[1],
        [PAYLOAD_OFFSET - 1] = ((uint8_t *)&type)[0],

        [PAYLOAD_OFFSET + 0] = 4 | (ihl << 4),
        [PAYLOAD_OFFSET + 2] = 0,
        [PAYLOAD_OFFSET + 3] = IPV4_LENGTH,
        [PAYLOAD_OFFSET + 9] = protocol,
    };
    const size_t buf_len = sizeof(buf);
    struct Ipv4Info info; 
    if ( true == ethIpv4Parse ( buf, buf_len, &info) &&
         protocol == info.protocol &&
         info.optionsPresent)
    {
        return true;
    }
    return false;

    #undef PAYLOAD_OFFSET
    #undef IPV4_LENGTH
    #undef CRC_SIZE
}


bool IPv4_as_payload_of_Ethernet_frame_with_3_VLAN_ethertypes()
{
    // https://en.wikipedia.org/wiki/IEEE_802.1Q
    printf("%s\n", __FUNCTION__);
    #define PAYLOAD_OFFSET (14 + 4*3) 
    #define IPV4_LENGTH 46
    #define CRC_SIZE 4
    const uint8_t protocol = 0x11;
    const uint8_t ihl = 6;
    const uint16_t vlan_QinQ_type = 0x88a8;
    const uint16_t vlan_type = 0x8100;
    const uint16_t type = 0x0800;

    const uint8_t buf[PAYLOAD_OFFSET + IPV4_LENGTH + CRC_SIZE]= {

        [PAYLOAD_OFFSET - 3*4 - 2] = ((uint8_t *)&vlan_QinQ_type)[1],
        [PAYLOAD_OFFSET - 3*4 - 1] = ((uint8_t *)&vlan_QinQ_type)[0],

        [PAYLOAD_OFFSET - 2*4 - 2] = ((uint8_t *)&vlan_QinQ_type)[1],
        [PAYLOAD_OFFSET - 2*4 - 1] = ((uint8_t *)&vlan_QinQ_type)[0],

        [PAYLOAD_OFFSET - 1*4 - 2] = ((uint8_t *)&vlan_type)[1],
        [PAYLOAD_OFFSET - 1*4 - 1] = ((uint8_t *)&vlan_type)[0],
        
        [PAYLOAD_OFFSET - 2] = ((uint8_t *)&type)[1],
        [PAYLOAD_OFFSET - 1] = ((uint8_t *)&type)[0],

        [PAYLOAD_OFFSET + 0] = 4 | (ihl << 4),
        [PAYLOAD_OFFSET + 2] = 0,
        [PAYLOAD_OFFSET + 3] = IPV4_LENGTH,
        [PAYLOAD_OFFSET + 9] = protocol,
    };
    const size_t buf_len = sizeof(buf);
    struct Ipv4Info info; 
    if ( true == ethIpv4Parse ( buf, buf_len, &info) &&
         protocol == info.protocol &&
         info.optionsPresent)
    {
        return true;
    }
    return false;

    #undef PAYLOAD_OFFSET
    #undef IPV4_LENGTH
    #undef CRC_SIZE
}

void main()
{
    // assert(false == assert_check());
    // assert(false == IPv4_frame_length_more_than_20());
    // assert(false == IPv4_frame_length_less_than_65535());
    // assert(false == IPv4_version_4());
    // assert(false == IPv4_total_length_same_to_buff_size());
    // assert(true == IPv4_protocol_check());
    // assert(true == IPv4_options_are_present());
    // assert(true == IPv4_as_payload_of_Ethernet_frame_without_VLAN());
    // assert(true == IPv4_as_payload_of_Ethernet_frame_with_1_VLAN_ethertype());
    // assert(true == IPv4_as_payload_of_Ethernet_frame_with_3_VLAN_ethertypes());

    #include "test_data.h"
    typedef struct TestFrame {
        const uint8_t *buf;
        const char *name;
        const size_t len;
        const bool truefalse;
        struct Ipv4Info i;
    }
    TestFrame;

    #define T(x) x, #x, sizeof(x)

    TestFrame frames[] = {
        [0] = { T(IPv4_ACK_frame), true, .i = { 0, 0, false} },
    };
    const size_t test_frame_len = sizeof(frames)/sizeof(frames[0]);
    for (size_t i = 0; i < test_frame_len; ++i)
    {
        TestFrame *frame = &frames[i];
        struct Ipv4Info info = {0};
        printf("%s", frame->name);
        bool ret = ethIpv4Parse ( frame->buf, frames->len, &info);
        assert ( ret == frames->truefalse);
        if (ret)
            assert( true ); /* This needs to check validity of returned Ipv4Info */
    }
}